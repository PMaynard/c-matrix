#include <stdio.h>
#include <curl/curl.h>
#include <ucl.h>
#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "main.h"

/* A test case that does nothing and succeeds. */
static void null_test_success(void **state) {
	// fail();
    (void) state; /* unused */
}
int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(null_test_success),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
