#include <stdio.h>
#include <curl/curl.h>
#include <ucl.h>
#include <string.h>
#include <syslog.h>

#include "json.h"

#include "main.h"

void printLog(int facility_priority, const char *s){
	openlog ("c-matric", LOG_CONS, LOG_USER);
	syslog (LOG_MAKEPRI(LOG_USER, facility_priority), "%s", s);
	printf("[%d] %s\n", facility_priority, s);
}

void init_string(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
	fprintf(stderr, "malloc() failed\n");
	exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t write_data(void *buffer, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size*nmemb;
	s->ptr = realloc(s->ptr, new_len+1);

	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}

	memcpy(s->ptr+s->len, buffer, size*nmemb);
	
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}

static void dump(const char *text, FILE *stream, unsigned char *ptr, size_t size)
{
  size_t i;
  size_t c;
  unsigned int width=0x10;
 
  fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
          text, (long)size, (long)size);
 
  for(i=0; i<size; i+= width) {
    fprintf(stream, "%4.4lx: ", (long)i);
 
    /* show hex to the left */
    for(c = 0; c < width; c++) {
      if(i+c < size)
        fprintf(stream, "%02x ", ptr[i+c]);
      else
        fputs("   ", stream);
    }
 
    /* show data on the right */
    for(c = 0; (c < width) && (i+c < size); c++) {
      char x = (ptr[i+c] >= 0x20 && ptr[i+c] < 0x80) ? ptr[i+c] : '.';
      fputc(x, stream);
    }
 
    fputc('\n', stream); /* newline */
  }
}
 
static int my_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp)
{
  const char *text;
  (void)handle; /* prevent compiler warning */
  (void)userp;
 
  switch (type) {
  default: /* in case a new one is introduced to shock us */
    fprintf(stderr, "== Info: %s", data);
    return 0;
 
  case CURLINFO_HEADER_OUT:
    text = "=> Send header";
    break;
  case CURLINFO_DATA_OUT:
    text = "=> Send data";
    break;
  case CURLINFO_SSL_DATA_OUT:
    text = "=> Send SSL data";
    break;
  case CURLINFO_HEADER_IN:
    text = "<= Recv header";
    break;
  case CURLINFO_DATA_IN:
    text = "<= Recv data";
    break;
  case CURLINFO_SSL_DATA_IN:
    text = "<= Recv SSL data";
    break;
  }

  dump(text, stderr, (unsigned char *)data, size);
  return 0;
}

int main(int argc, char *argv[])
{
	// Misc
	char * server = NULL; 
	bool verbose = 0;

	// CURL
	CURL *curl;
	CURLcode res;
	struct string s;
	init_string(&s);

	struct curl_slist *headers = NULL;
	
	headers = curl_slist_append(headers, "Accept: application/json");
	headers = curl_slist_append(headers, "Content-Type: application/json");
	
	// JSON
	json_object *json_res, *versions, *versions_obj;

	// CONFIG
	struct ucl_parser *parser = NULL;
	ucl_object_t *obj = NULL;
	
	// 1. Load the configuration file
	parser = ucl_parser_new (0);
	ucl_parser_add_file(parser, "c-matrix.conf");

	if (ucl_parser_get_error (parser)) {
		printLog(LOG_ERR, ucl_parser_get_error (parser));
	}
	else {
	    obj = ucl_parser_get_object(parser);
	}

	// 1.1 Find the server address
	ucl_object_iter_t it;
	const ucl_object_t *cur;

	it = ucl_object_iterate_new (obj);
	while ((cur = ucl_object_iterate_safe (it, true)) != NULL) {
		if (strcmp(ucl_object_key(cur), "server") == 0) {
			printf("%s = %s\n", ucl_object_key(cur), ucl_object_tostring(cur));
			server = malloc (strlen(ucl_object_tostring(cur)) + 1);
			strcpy(server, ucl_object_tostring(cur)); // TODO: Potential leak of memory pointed to by 'server'
		}

		if (strcmp(ucl_object_key(cur), "verbose") == 0) {
			printf("%s = %d\n", ucl_object_key(cur), ucl_object_toboolean(cur));
			verbose = ucl_object_toboolean(cur);
		}
	}

	ucl_object_iterate_free (it);
	if (parser != NULL) {
		ucl_parser_free (parser);
	}
	if (obj != NULL) {
		ucl_object_unref (obj);
	}

	// 2. Set up CURL 
	curl = curl_easy_init();
	if(curl && server != NULL) {
		curl_easy_setopt(curl, CURLOPT_URL, server);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		if(verbose == 1){
			curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		}
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); 
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);

		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK){
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}

		// printf("PRINT> %s\n", s.ptr);

		// Parse results.
		json_res = json_tokener_parse(s.ptr);
		if (json_res){
			printf("Result> %s\n", json_object_to_json_string(json_res));

			// Loop over versions.
			versions = json_object_object_get(json_res, "versions");
			int arraylen = json_object_array_length(versions);

			printf("Supported Versions:\n");
			for (int i = 0; i < arraylen; i++) {
			  // get the i-th object in medi_array
			  versions_obj = json_object_array_get_idx(versions, i);
			  printf("  Version=%s\n", json_object_get_string(versions_obj));
			}
		}else{
			printf("No response, check configuration.\n");
		}

		// Free variables
		json_object_put(json_res);
		curl_easy_cleanup(curl);
	}
	free(server);
	free(s.ptr);
	curl_slist_free_all(headers);
	closelog ();
	printf("EOF\n");

	return 0;
}
