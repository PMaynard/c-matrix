

struct string {
  char *ptr;
  size_t len;
};

void init_string(struct string *s);
size_t write_data(void *buffer, size_t size, size_t nmemb, struct string *s);

