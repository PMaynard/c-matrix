C-Matrix
========

Dependencies

- [meson](http://mesonbuild.com/) For building and dependency checking.
- [libcurl](https://curl.haxx.se/libcurl/) For HTTP requests.
- [json-c](https://github.com/json-c/json-c) For parsing JSON.
- [libucl](https://github.com/vstakhov/libucl) For parsing configuration files.

To build

	meson build
	ninja -C build
	ninja -C build scan-build # static analysis.
	ninja -C build test # Run tests

To run

	build/c-matrix 

Output

	server = https://matrix.org/_matrix/client/versions
	Result> { "versions": [ "r0.0.1", "r0.1.0", "r0.2.0", "r0.3.0" ] }
	Supported Versions:
	  Version=r0.0.1
	  Version=r0.1.0
	  Version=r0.2.0
	  Version=r0.3.0
	EOF

Debug Output

	journalctl -f  /home/.../c-matrix/build/c-matrix 
	Jul 06 15:45:45 ecit01140u c-matric[22032]: cannot open file c-matrix.confs: No such file or directory
